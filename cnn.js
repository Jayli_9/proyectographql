const pgPromise = require('pg-promise');

const pgp = pgPromise({}); // Empty object means no additional config required

const config = {
    host: 'localhost', //process.env.POSTGRES_HOST,
    port: '5432', //process.env.POSTGRES_PORT,
    database: 'pizza', //process.env.POSTGRES_DATABASE,
    user: 'postgres', //process.env.POSTGRES_USER,
    password: '123456789' //process.env.POSTGRES_PASSWORD
};

// const db = pgp(config);

// // db.any('select * from ingredient')
// //    .then(res => { console.log(res); });
const db =pgp(config);

db.any('select * from pizza')
.then(res=> { 
    console.log(res);

});
exports.db = db;