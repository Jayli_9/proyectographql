const { db } = require("../cnn");

const pizzaResolver = {
    Query: {
        pizzas(root, { name }) {
            if (name === undefined) {
                return executeQuery(`SELECT * FROM pizza`, name);
            }
            let dataPizzas = resolveListPizzas(name);
            return dataPizzas;
        }
    },
    Pizza: {
        ingredients(pizza1) { // Have to resolve the field "ingredients" when a "Pizza" object is requested
            const query = `SELECT ingredients.* FROM pizza_ingredients, ingredients 
            WHERE pizza_ingredients.ingredient_id = ingredients.id 
                and pizza_ingredients.pizza_id = $1;`;
            return executeQuery(query, pizza1.id);
        }
    },
    Mutation: {
        createPizzas(root, { pizzas }) {
            if (pizzas === undefined || pizzas.length < 1) {
                // return '[no hay pizza]';
                return null;
            }
            let newPizzas = [];
            pizzas.forEach((pizza) => {
                let newPizzas1 = [];
                if (pizza.name) {
                    const query = `INSERT INTO pizza(name, origin) VALUES ($1, $2) returning id;`;
                    db.one(query, [pizza.name, pizza.origin])
                        .then(res => {
                            let newPizza = {
                                id: res.id,
                                name: pizza.name,
                                origin: pizza.origin
                            };
                            console.log('Este es el registro nuevo: ' + newPizza);
                            newPizzas1.push(newPizza);
                            if (pizza.ingredientIds && pizza.ingredientIds.length > 0) {
                                pizza.ingredientIds.forEach((ingredientId) => {
                                    const query = `INSERT INTO pizza_ingredients(pizza_id, ingredient_id) VALUES ($1, $2)`;
                                    executeQuery(query, [res.id, ingredientId])
                                });
                            }
                        })
                        .catch(err => err);
                    console.log(newPizzas1);
                }
            });
            newPizzas = newPizzas.length > 0 ? newPizzas : null;
            // The "ingredients" field is autoresolved by GraphQL
            return newPizzas;
        }, updatePizzas(root, { pizzas }) {
            if (pizzas === undefined || pizzas.length < 1) {
                return null;
            }
            pizzas.forEach((pizza) => {
                if (pizza.id) {
                    const query = `UPDATE pizza SET name=$2, origin=$3 WHERE id=$1;`;

                    // console.log(query, pizza);
                    db.any(query, [pizza.id, pizza.name, pizza.origin])
                        .then(res => {
                            console.log('OK: update pizza id ' + pizza.id);
                        })
                        .catch(err => {
                            console.log('error update: ' + err);
                        });
                }
            });


            for (let index = 0; index < pizzas.length; index++) {
                const element = pizzas[index];
                const eliminar = `DELETE FROM public.pizza_ingredients WHERE pizza_id=$1;`
                db.any(eliminar, [element.id])
                    .then(res => {
                        console.log('OK: Eliminado pizza id ' + element.id);
                    })
                    .catch(err => {
                        console.log('error Eliminar: ' + err);
                    });
                const query1 = `INSERT INTO pizza_ingredients(pizza_id, ingredient_id) VALUES ($1, $2);`
                for (let index1 = 0; index1 < element.ingredientIds.length; index1++) {
                    const element1 = element.ingredientIds[index1];
                    db.any(query1, [element.id, element1])
                        .then(res => {
                            console.log('OK: update pizza id ' + element.id);
                        })
                        .catch(err => {
                            console.log('error update: ' + err);
                        });
                }

            }
            let newPizzas = resolveListPizzas(pizzas);
            console.log("Nuevas");
            console.log(newPizzas);
         
            return newPizzas;


        }, deletePizzas(root, { id }) {
            let query;
            let query1;
        //    console.log('Preuba valor'+ query)
        //    console.log('Preuba valor'+ query1)

            if (id === undefined) {
                return "Falta Parámetro";
            } else {
                query1 = `DELETE FROM public.pizza_ingredients WHERE pizza_id=$1;`;
                query = `DELETE FROM public.pizza WHERE id=$1;`;
              
            }
            db
                .any(query1, id)
                .then(res => {
                })
                .catch(err => err);
            db
                .any(query, id)
                .then(res => {
                })
                .catch(err => err);

            return "Eliminado";
        }
    }
};

// Create functions 
async function resolveListPizzas(listPizzas) {
    const ids = [listPizzas];
    const query = `SELECT * FROM pizza WHERE name IN ($1:csv)`;
    let dataListPizzas = await db.any(query, ids)
        .then(res => res)
        .catch(err => err);
    return dataListPizzas;
}

async function executeQuery(query, parameters) {
    let recordSet = await db.any(query, parameters)
        .then(res => res)
        .catch(err => err);
    return recordSet;
}

export default pizzaResolver;
