import schema from './schema.graphql';
import Query from './Query.graphql';
import Pizza from './Pizza.graphql';
import Ingredient from './Ingredient.graphql';
import Mutation from './Mutation.graphql';

//const { db } = require("../cnn");
//console.log(db);

const typeDefs = [
    schema,
    Query,
    Pizza,
    Ingredient,
    Mutation

];

export default typeDefs;